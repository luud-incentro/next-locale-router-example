const next = require("next")
const express = require("express")

const { config, createLocaleMiddleware } = require("@incentro/next-locale-router")

const dev = process.env.NODE_ENV !== "production"
const app = next({ dev })
const handle = app.getRequestHandler()

const localeMiddleware = createLocaleMiddleware(config, app)

app.prepare().then(() => {
  const server = express()

  server.set("trust proxy", true)
  server.use(localeMiddleware)

  server.all("*", (request, response) => handle(request, response))

  server.listen(3000, (err) => {
    if (err) throw err
    console.log("> Ready on http://localhost:3000")
  })
})
