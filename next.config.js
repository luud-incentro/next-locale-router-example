const { config } = require("@incentro/next-locale-router")

module.exports = {
  ...config.toNextConfig(),
}
