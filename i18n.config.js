const dutchDomain = {
  hostname: "nextjs.dutch",
  defaultLocale: "nl",
  subpaths: [
    {
      locale: "nl",
      path: "/",
    },
  ],
}

const belgianDomain = {
  hostname: "nextjs.belgian",
  defaultLocale: "nl-BE",
  subpaths: [
    {
      locale: "nl-BE",
      path: "/nl/",
    },
    {
      locale: "fr",
      path: "/fr/",
    },
  ],
}

const domains = [dutchDomain, belgianDomain]
const defaultLocale = "nl"

module.exports = {
  domains,
  defaultLocale,
}
