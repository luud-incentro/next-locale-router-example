import { GetStaticProps, InferGetStaticPropsType } from "next"
import Link from "@incentro/next-locale-router/link"
import Layout from "../components/Layout"

const AboutPage = ({ locale }: InferGetStaticPropsType<typeof getStaticProps>) => (
  <Layout title="About | Next.js + TypeScript Example">
    <h1>About</h1>
    <p>This is the about page</p>
    <p>
      <Link href="/">
        <a>Go home</a>
      </Link>
      <p>Detected locale: {locale}</p>
    </p>
  </Layout>
)

export const getStaticNames = async () => {
  return {
    names: [
      {
        name: "over",
        locale: "nl",
      },
      {
        name: "qui-sommes-nous",
        locale: "fr",
      },
    ],
  }
}

export const getStaticProps: GetStaticProps = async ({ locale }) => {
  return {
    props: { locale },
  }
}

export default AboutPage
