import { GetStaticProps, InferGetStaticPropsType } from "next"
import Link from "@incentro/next-locale-router/link"
import Layout from "../components/Layout"

const NewPage = ({ locale }: InferGetStaticPropsType<typeof getStaticProps>) => (
  <Layout title="New | Next.js + TypeScript Example">
    <h1>New</h1>
    <p>This is the new page</p>
    <p>
      <Link href="/">
        <a>Go home</a>
      </Link>
      <p>Detected locale: {locale}</p>
    </p>
  </Layout>
)

export const getStaticProps: GetStaticProps = async ({ locale }) => {
  return {
    props: { locale },
  }
}

export default NewPage
