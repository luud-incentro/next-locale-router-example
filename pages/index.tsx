import { GetServerSideProps, InferGetServerSidePropsType } from "next"
import Link from "@incentro/next-locale-router/link"
import { FunctionComponent } from "react"
import Layout from "../components/Layout"

const IndexPage: FunctionComponent<InferGetServerSidePropsType<typeof getServerSideProps>> = ({
  locale,
  defaultLocale,
  locales,
}) => (
  <Layout title="Home | Next.js + TypeScript Example">
    <h1>Hello Next.js 👋</h1>
    <p>
      <Link href="/about">
        <a>About</a>
      </Link>
      <p>Detected locale: {locale}</p>
      <p>Detected defaultLocale: {defaultLocale}</p>
      <p>Detected locales: {locales?.join(", ")}</p>
    </p>
  </Layout>
)

export const getServerSideProps: GetServerSideProps = async ({
  locale,
  defaultLocale,
  locales,
}) => {
  return {
    props: {
      locale,
      defaultLocale,
      locales,
    },
  }
}

export default IndexPage
